var _ = require("underscore");

// location def should be 
var locationMap = [{locationID: 1, locationName: '实验室'}, 
				 {locationID: 2, locationName: '足球场'}];

var nodeLocationMapList = [{nodeID: 32782, locationID: 1}];


exports.locationMap = function(){
	return locationMap;
};

exports.nodeLocationMapList = function(){
	return nodeLocationMapList;
};

exports.getLocationIDWithNodeID = function(_nodeID){
	var map = _.find(nodeLocationMapList, function(_map){
		return _map.nodeID == _nodeID;
	});
	if(map == null){
		return -1;
	}else{
		return map.locationID;
	}
};




