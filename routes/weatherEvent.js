/***********************************************


************************************************/
var _ = require("underscore");
var events = (require("./events").events)();
var locations = require("./locations").locationMap();
// var eventDefinations = require("./eventDefinations");
var eventDefinations = require("./events").eventDefinations();


exports.getEventDes = function(_para){
	var eventResults = findFitedEvents(_para);
	if(_.size(eventResults) <= 0){
		return null;
	}else{
        var event4Rtn = _.map(eventResults, function(_result){
            var location = _.find(locations, function(_location){
                return _location.locationID == _result.locationID;
            });
            if(location == null){
                location = {locationID: -1, locationName: '未定义'};
            }
            var eventDef = _.find(eventDefinations, function(_def){
                return _def.eventID == _result.eventID;
            });
            if(eventDef == null){
                eventDef = {eventID: -1, eventName: '无', eventDes: ''};
            }

            return {   eventName: eventDef.eventName
                        , locationName: location.locationName
                        , eventDes: eventDef.eventDes
                        , values: _para.values
                        , level: eventDef.level
                        , img: eventDef.img
                        , eventID:eventDef.eventID
                        , locationID: location.locationID
                    };
        });

		// var eventResult = eventResults[0];
        // console.log('trigger event => **************************');
        // console.dir(event4Rtn);

		// var location = _.find(locations, function(_location){
		// 	return _location.locationID == eventResult.locationID;
		// });
		// if(location == null){
		// 	location = {locationID: -1, locationName: '未定义'};
		// }
		// var eventDef = _.find(eventDefinations, function(_def){
		// 	return _def.eventID == eventResult.eventID;
		// });
		// if(eventDef == null){
		// 	eventDef = {eventID: -1, eventName: '无', eventDes: ''};
		// }
		// return eventResult;
        // var event4Rtn = {   eventName: eventDef.eventName
        //                     , locationName: location.locationName
        //                     , eventDes: eventDef.eventDes
        //                 };
        // console.log('trigger event => **************************');
        // console.dir(event4Rtn);
		return event4Rtn;
	}
};
exports.findFitedEvents = function(_para){
	return findFitedEvents(_para);
};

function findFitedEvents(_para){
    var Debug = false;

    if(Debug) console.log("start ...");
    if(Debug) console.log('para => ')
    if(Debug) console.dir(_para);
    var locationFitMap = _.filter(events, function(_event){
        if(Debug) console.log('event => ');
        if(Debug) console.dir(_event);
        if(_event.locationID != _para.locationID){
            if(Debug) console.log('locationID not fit ');
            return false;
        } 
        var logics = _event.logics;
        var logicResults = _.map(logics, function(_logic){
            if(Debug) console.log('current logic => ' + _logic);
            try{
                var jsstr = _.template(_logic, _para.values);
                if(Debug) console.log(jsstr + ' => ' + eval(jsstr));
                return eval(jsstr);                
            }catch(error){
                //可能抛出变量未定义的错误，说明该事件定义的需要的变量参数并没有全部包含，那也说明不能满足该事件
                if(Debug) console.log("info => " + error.message);
                return false;
            }
        });
        if((_.size(logicResults) == _.size(logics)) && !_.contains(logicResults, false)){
            return true;
        }else{
            if(Debug) console.log('not all results art true');
            return false;
        } 
    });
    // console.dir(locationFitMap);
    return locationFitMap;

}



