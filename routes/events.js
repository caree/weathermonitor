//event defi should be like this
// 如果该位置满足逻辑条件，就触发事件
// 溫度 舒適度的區間

// -35 ~ -10度: 防凍傷
// -10 ~ 17度: 寒冷
// 18 ~ 23度: 舒適溫度
// 24 ~ 40度: 炎熱
// 41 ~ 50度: 防暑
// 濕度 舒適度的區間

// 20 ~ 39 %: 乾燥
// 40 ~ 70 %: 舒適濕度
// 71 ~ 100 %: 潮溼

var events = 
		[
		    {locationID: 1, logics: ["<%= humidity %> <= 70", "<%= humidity %> > 40"], eventID: '0101'}
		  , {locationID: 1, logics: ["<%= humidity %> <= 40", "<%= humidity %> >= 20"], eventID: '0102'}
		  , {locationID: 1, logics: ["<%= humidity %> < 20"], eventID: '0103'}
		  , {locationID: 1, logics: ["<%= humidity %> > 70"], eventID: '0104'}

		  , {locationID: 1, logics: ["<%= temperature %> >= 40"], eventID: '0201'}
		  , {locationID: 1, logics: ["<%= temperature %> < 40", "<%= temperature %> >= 24"], eventID: '0202'}
		  , {locationID: 1, logics: ["<%= temperature %> < 24", "<%= temperature %> >= 18"], eventID: '0203'}
		  , {locationID: 1, logics: ["<%= temperature %> < 18", "<%= temperature %> >= -10"], eventID: '0204'}
		  , {locationID: 1, logics: ["<%= temperature %> < -10", "<%= temperature %> >= -35"], eventID: '0205'}
		  , {locationID: 1, logics: ["<%= temperature %> < -35"], eventID: '0206'}
		];

//编码规则  xxyy  xx:参数类型   yy:序列	
//               01:湿度  02:温度
var eventDefinations =
		[
			{eventID: '0101', eventName:'合适的湿度', eventDes: '这个湿度对身体太合适了，注意保持哦！', img: '', level: 1}, 
			{eventID: '0102', eventName:'实验室有点干燥', eventDes: '实验室有点干燥， 为了防止静电，打开加湿器吧！', img: '', level: 2}, 
			{eventID: '0103', eventName:'实验室太干燥了', eventDes: '紧急情况，实验室太干燥了！！！', img: '', level: 3}, 
			{eventID: '0104', eventName:'太潮湿了', eventDes: '实验室太潮湿了，是不是进水了？？', img: '', level: 2}, 
			// {eventID: 1, eventName:'太干燥', eventDes: '不冷，但是太干燥了，打开加湿器吧！'}, 
			{eventID: '0201', eventName: '实验室太热', eventDes: '太热了，别被烤熟了！！', img: '', level: 3}, 
			{eventID: '0202', eventName: '实验室比较热', eventDes: '比较热，打开空调凉快一下吧！', img: '', level: 2}, 
			{eventID: '0203', eventName: '实验室温度正合适', eventDes: '温度正合适哦！', img: '', level: 1}, 
			{eventID: '0204', eventName: '实验室有点冷', eventDes: '有点冷，打开空调吧', img: '', level: 2}, 
			{eventID: '0205', eventName: '冰窖实验', eventDes: '如果你在练习挨冻，这个温度不错', img: '', level: 3}, 
			{eventID: '0206', eventName: '冻僵了', eventDes: '还能有活人吗？', img: '', level: 3}, 
			{eventID: -1, eventName: '无', eventDes: '', img: '', level: 1}
		];

var properties = ['temperature', 'humidity'];

exports.events = function(){
	return events;
};


exports.eventDefinations = function(){
	return eventDefinations;
};

exports.properties = function(){
	return properties;
};




