
var http = require('http');
var WebSocketServer = require('ws').Server;
var Node = require('./Node');
var timeFormater = require('./timeFormat').getCurrentTime;

//*****************************************************************************

nodeList= [];

var server;
global.wss;
var clients = [];


exports.startWebSocketServer = function(app){

	server = http.createServer(app);
	global.wss = new WebSocketServer({server : server});
	global.wss.broadcast = function(data) {
	    for(var i in this.clients)
	        this.clients[i].send(data);
	};
	wss.on('connection', function(ws){
		clients.push(ws);

		    console.log('connected'.info);
		    var obj = {name:'nodes', content:JSON.stringify(nodeList)};
		    var str = JSON.stringify(obj);
		    console.log(str.data);
		    ws.send(str);

		ws.on('open', function(){
		    // console.log('connected');
		    // var obj = {name:'nodes', content:JSON.stringify(global.nodeList)};
		    // var str = JSON.stringify(obj);
		    // console.log(str);
		    // ws.send(str);
		  });
		
		ws.on('message', function(msg){
		    console.log(('message => '+ msg).data);
		    try{
			    var objMsg  = JSON.parse(msg);
			    if(objMsg.cmd == 'weatherNodeInfo'){
			    	var nodeInfo = JSON.parse(objMsg.data);
			    	// console.dir(nodeInfo);
			    	updateNodeList(nodeInfo);
			    }
		    }
		    catch(_err){
		    	console.error(('on message => ' + _err).error);
		    }
		    // wss.broadcast('broadcast => ' + msg);
		  });
		
		ws.on('close', function(){
		    var index = clients.indexOf(ws);
		    clients.splice(index, 1);
		    console.log(('close =>').error);
		  });
	  
	});	

	return server;
}


function updateNodeList(node){
	if(node == null) return;
	var nodeSameID = findNode(node);
	if(nodeSameID != null){
		console.log(nodeSameID.data);
		var b = nodeSameID.update(node);
		if(true == b){
			// todo broadcast
			console.log((timeFormater() + '  node update => ').info);
			var str = JSON.stringify([node]);
			console.log(str.data);
			broadcastNodeInfo(str);
			// console.log('********************************');
			// console.log(JSON.stringify(nodeList));
		}
	}else{
		var newNode = new Node(node)
		nodeList.push(newNode);
		console.log((timeFormater() + '  new node => ').info);
		var str = JSON.stringify([newNode]);
		console.log(str.data);
		broadcastNodeInfo(str);
		// todo broadcast		
		// console.log('********************************');
		// console.log(JSON.stringify(global.nodeList));
		// console.dir(global.nodeList);

	}
}

function broadcastNodeInfo(msg){
	var obj = {name:'nodes', content:msg};
	global.wss.broadcast(JSON.stringify(obj));
}
function findNode(node){
	for(var n in nodeList){
		if(nodeList[n].nodeId == node.nodeId){
			return nodeList[n];
		}
	}
	return null;
}



