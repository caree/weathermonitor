
var _ = require("underscore");

// var eventList = [];
var eventList =
[ { eventName: '实验室太干燥了',
    locationName: '实验室',
    eventDes: '实验室太干燥了，赶紧逃离吧！！！',
    values: { temperature: 20.55, humidity: 14.32 },
    level: 3,
    img: '',
    eventID: '0103',
    locationID: 1 },
  { eventName: '实验室有点冷',
    locationName: '实验室',
    eventDes: '有点冷，打开空调吧',
    values: { temperature: 20.55, humidity: 14.32 },
    level: 2,
    img: '',
    eventID: '0204',
    locationID: 1 } ];	
exports.updateEvent = function(_events){
	_.each(_events, function(_event){
		var existedEvent = _.find(eventList, function(_existedEvent){
			return (_existedEvent.eventID == _event.eventID);
		});
        if(existedEvent != null){
            if(existedEvent.values != _event.values){//已经有了，则更新数据
                existedEvent.values = _event.values;
            }            
        }else{
            eventList = _.filter(eventList, function(_evt){//需要把同类的事件先删除
                return (_evt.eventID.substr(0,2) != _event.eventID.substr(0,2));
            });
            eventList.push(_event);
        }
	});
};
exports.getEvents = function(){
	return eventList;
};








