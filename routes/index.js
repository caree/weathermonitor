/*
 * GET home page.
 */

var _ = require("underscore");
var nodeMap = require('./nodeMap');
var eventList = require('./eventList');
var fs = require('fs');
var fs_extended = require("fs-extended");
var datejs = require("datejs");

//****************************************************
var PUBLIC_IMAGE_FOLDER_PATH = "./public/images/cameraImg/";
var newestPicNameTemp = [];


// startVideoMonitorSever();

function startVideoMonitorSever(){
	initialNewestPicNameTemp();
	setInterval(intervalToUpdateNewestPicName, 8000);
	setInterval(intervalToDeleteGarbagePics, 1000 * 10);//every hour
	// setInterval(intervalToDeleteGarbagePics, 1000 * 60 * 60);//every hour

}
exports.index = function(req, res){
	var content = JSON.stringify(G_itemList);
	res.render('index', { _title: '环境监控', _itemList: content});
};
exports.wsTest = function(req, res){
	// res.render('wsTest', {_nodeDes: JSON.stringify(nodeMap.getNodeDes([32778, 32775]))});
	var nodesDes = nodeMap.getNodeDes([32780, 32775, 32779, 32778, 32777]);
	console.log(nodesDes);
	res.render('wsTest', {_nodeDes: JSON.stringify(nodesDes)});
};

exports.wsTest_ie10 = function(req, res){
	// res.render('wsTest', {_nodeDes: JSON.stringify(nodeMap.getNodeDes([32778, 32775]))});
	var nodesDes = nodeMap.getNodeDes([32780, 32775, 32779, 32778, 32777, 32776, 32781]);
	console.log(nodesDes);
	console.dir(req.headers['user-agent']);
	var user_agent = req.headers['user-agent'];
	if(user_agent.search(/MSIE/) >= 0){
		console.log('this request from IE');
	}
	else{
		console.log('this request NOT from IE');
	}
	// Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)
	// Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.62 Safari/537.36
	res.render('wsTest_ie10', {_nodeDes: JSON.stringify(nodesDes)});
};

exports.map = function(req, res){
	res.render('map_location');
};
exports.bwumap = function(req, res){
	res.render('bwumap');
};

exports.allmap = function(req, res){
	res.render('map_location_all');
};
exports.nodeInfo = function(req, res){
	var id = req.params.nodeID;
	console.log(('nodeInfo => id = ' + id).data);
	var locationID = id.split('_',2)[0];
	var nodeInfo = nodeMap.getNodeID(id);
	console.log(('request mapID => ' + id + ' need Node ' + nodeInfo.nodeID).data);

	var nodesDes = nodeMap.getNodeDes(nodeInfo.nodeID);
	var sensorType = nodeMap.getSensorType();
	// console.log(nodesDes);

	//如果是IE，使用Ajax方案
	var user_agent = req.headers['user-agent'];
	if(user_agent.search(/MSIE/) >= 0){
		console.log('this request from IE');
		res.render('nodeInfo_ajax', 
					{_title: nodeInfo.name
						, _nodeDes: JSON.stringify(nodesDes)
						, _sensorType: JSON.stringify(sensorType)
						, _locationID: locationID});
	}
	else{
		console.log('this request NOT from IE');
		res.render('nodeInfo', 
					{_title: nodeInfo.name
						, _nodeDes: JSON.stringify(nodesDes)
						, _sensorType: JSON.stringify(sensorType)
						, _locationID: locationID});
	}	
};
exports.ajaxGetNodeInfo = function(req, res){
	var content = JSON.stringify(global.nodeList);
	console.log('ajaxGetNodeInfo => ' + content);
	var obj = {name:'nodes', content: content};
	res.send(JSON.stringify(obj));
};
exports.eventInfoIndex = function(req, res){
	res.render('eventInfoIndex', {_title:''});
};
exports.eventInfo = function(req, res){
	var content = JSON.stringify(eventList.getEvents());
	res.render('eventInfo_ajax', {_title: '', _eventList: content});
}
exports.ajaxGetEvents = function(req, res){
	var content = JSON.stringify(eventList.getEvents());
	res.send(content);
};
exports.realtime_camera_full = function(req, res){
	var id = req.params.id;
	var camera_text = get_camera_text_by_id(id);

	res.render('index_realtime_camera_full', {pic_name:'rt', camera_id:id, camera_text:camera_text});
};
exports.locationID = function(req, res){
	var locationID = req.params.locationID;
	var location = _.find(G_itemList, function(_item){
		return _item.itemID == locationID;
	});
	if(null == location){
		res.send('系统异常');
	}else{
        var markParaList = _.filter(G_markParaList, function(_markPara){
        	return _markPara.itemID == locationID;
        });
		res.render(locationID+'_map', {_markParaList: JSON.stringify(markParaList)});
	}
}

exports.getNewestPicName = function(req, res){
	var picNameReturn = 'rt.png';
	var preId = req.params.id;
	var temp = _.find(newestPicNameTemp, function(_temp){
		return _temp.id == preId;
	});
	if(temp != null){
		if(temp.pic == "rt.png"){
			picNameReturn = temp.pic;
		}else{
			picNameReturn = temp.ip + '/' + temp.pic;
		}
	} 
	// console.log(picNameReturn);
	// console.dir(newestPicNameTemp);
	res.send(picNameReturn);
	return;
};

function initialNewestPicNameTemp(){
	// return;
	newestPicNameTemp = _.map(pathName_IP, function(_ip){
		var id_ip_map = _.find(ip_id_map_list, function(_id_ip_map){
			return _id_ip_map.ip == _ip;
		});
		return {pic: 'rt.png', ip: _ip, id: id_ip_map.id};
	});

	// console.dir(newestPicNameTemp);
}

function intervalToDeleteGarbagePics(){
    var names=[];
    for(var i = -6; i<=0; i++){
        var dayName = (Date.today().add(i).days().toString("yyyy-MM-dd"));
        // console.log(dayName);
        names.push(dayName);
    }	
	deleteSrcFolderPics(names);
	deletePublicFolderPics();
}
function deletePublicFolderPics(){
    var today = Date.today();
    _.each(pathName_IP, function(_ip){
    	var path = PUBLIC_IMAGE_FOLDER_PATH + "/" + _ip;
    	if(fs.existsSync(path)){
		    var allFiles = fs.readdirSync(path);
		    // console.dir(allFiles);
		    _.each(allFiles, function(_pic){
		        var date = parseFileNameToDate(_pic);
		        if(date != null){
		            if(!Date.equals(date.clearTime(), today)){
		                fs_extended.deleteFileSync(path + "/" + _pic);
		                console.log(_pic + " has been deleted");
		            }
		        }
		    });    		
    	}
    });

}
function deleteSrcFolderPics(_ignore_folders){
    _.each(pathName_IP, function(_ip){
        var path2IP = baseDirectory + _ip;
        if(fs.existsSync(path2IP)){
            var allFiles = fs.readdirSync(path2IP);
            // console.dir(allFiles);
            _.each(allFiles, function(_folder){
                if(!_.contains(_ignore_folders, _folder)){
                    fs_extended.deleteDirSync(path2IP + "\\" + _folder);
                    console.log(_folder + '  has been deleted');
                }
            });
        }        
    });
}

//每隔一段时间更新至最新的图片
function intervalToUpdateNewestPicName(){
	console.log('intervalToUpdateNewestPicName => ');
	_.each(newestPicNameTemp, function(_ip_id_map){

		var newestFileName = getNewestFile(_ip_id_map.ip);
		console.log(_ip_id_map.ip + ' got new file ' + newestFileName);
		if(newestFileName != null){
            var seconds = getElapsedSeconds(parseFileNameToDate(newestFileName));
            if(seconds <= max_pic_update_interval){
                // console.log("this file updated in 10 seconds");
                if(copyPicToPublicFolder(newestFileName, _ip_id_map.ip)){
	                _ip_id_map.pic = newestFileName;
	                console.log('ip ' + _ip_id_map.ip + '   pic updated to ' + newestFileName);
                }
            }else {
            	_ip_id_map.pic = 'rt.png';
                console.log('ip ' + _ip_id_map.ip + '   pic reset to rt.png');
            }
		}
	});
	// console.dir()
}
function copyPicToPublicFolder(_name, _ip){
    var dayName = (Date.today().toString("yyyy-MM-dd"));
	var srcpath = baseDirectory + _ip + "\\" + dayName + "\\" + "01\\" + _name;
	var desPath = PUBLIC_IMAGE_FOLDER_PATH + _ip + "\\" + _name;
	try{
	    fs_extended.copyFileSync(srcpath, desPath);
	}catch(err){
		console.log('something wrong when copy file!');
		return false;
	}
    return true;
}
function getNewestFile(_ip){
    var path2IP = baseDirectory + _ip;
    if(fs.existsSync(path2IP)){
        var dayName = (Date.today().toString("yyyy-MM-dd"));
        var path2Date = path2IP + "\\" + dayName;
        if(fs.existsSync(path2Date)){
            var path2Chnl = path2Date + "\\" + "01";
            if(fs.existsSync(path2Chnl)){
                var allFiles = fs.readdirSync(path2Chnl);
                if(_.size(allFiles) > 0){
                    var newestFile = filterNewestFile(allFiles);
                    console.log('the newest file is ' + newestFile);  
                    return newestFile;
                }
            }
        }
    }
    return null    
}

function filterNewestFile(_allFiles){
    var newestFile = _.min(_allFiles, function(_file){
        return getElapsedSeconds(parseFileNameToDate(_file));
    });
    return newestFile;
}
function getElapsedSeconds(_date){
    if(null == _date) return 10000;
    return  (new Date()).getTime()/1000 - _date.getTime()/1000;
}
function parseFileNameToDate(_name){
    if(_name == null) return null;
    var splits = _name.split("_", 3);
    if(_.size(splits) >= 3){
        var timeString = splits[2];
        var time = Date.parse(splits[1], "yyyyMMdd");
        time.setHours(timeString.substr(0,2), timeString.substr(2,2), timeString.substr(4,2));
        return time;
    }
    return null;
}


//****************************************************
function get_camera_text_by_id(_id){
	return nodeMap.getCameraText(_id);
}
