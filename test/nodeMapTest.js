
/*
 * GET users listing.
 */
require("should");
var _ = require("underscore");
var nodeMap = require('../routes/nodeMap');


describe("nodeMap => ", function() {
	it('getNodeDes() ', function(){
		var desList = nodeMap.getNodeDes([]);
		(_.size(desList) == 0).should.be.true;

		var desList = nodeMap.getNodeDes([32770]);
		(desList != null).should.be.true;
		(desList.length == 1).should.be.true;
		(desList[0].des == '科技楼气象站').should.be.true;

		var desList = nodeMap.getNodeDes([32770,32769]);
		// console.dir(desList);
		(desList.length == 2).should.be.true;
		// (desList[0].des == '科技楼气象站').should.be.true;

	});

	it('getNodeID', function(){
		var ids = nodeMap.getNodeID(200);
		(_.size(ids) == 0).should.be.true;

		var ids = nodeMap.getNodeID(2);
		// console.log(ids);
		(_.size(ids.nodeID) == 1).should.be.true;

		var ids = nodeMap.getNodeID(1);
		// console.log(ids);
		(_.size(ids.nodeID) == 6).should.be.true;
		(_.contains(ids.nodeID, 32777)).should.be.true;
	});
});