require("should");
var _ = require("underscore");
var indexJS = require('../routes/index');
var datejs = require("datejs");
var fs = require("fs");
var fs_extended = require("fs-extended");

var baseDirectory = "C:\\program\\nodejs\\weathermonitor\\testPath\\";
var pathName_IP = ['192.168.1.108', '192.168.1.109', '192.168.1.110', '192.168.1.112', '192.168.1.113'];


describe("indexJS test => ", function() {
    // it('getNewestPicName ', function(){
    //     var req = {params:{id:'10001'}};
    //     var res = {send: function(str){}};
    //     var fileName = indexJS.getNewestPicName(req, res);

    // });
    // it('getNewestPicName => ', function(){
    //     _.each(pathName_IP, function(_ip){
    //         var newestFileName = getNewestFile(_ip);
    //         var seconds = getElapsedSeconds(parseFileNameToDate(newestFileName));
    //         if(seconds <= 10){
    //             console.log("this file updated in 10 seconds");
    //         }
    //         if(seconds <= 10000){
    //             console.log("this file updated in 10000 seconds");

    //         }
    //     });
    // });
    // it('copy => ', function(){
    //     copyPicToPublicFolder("ch01_20140112_092726_T_381.jpg", "192.168.1.112");
    // });

    // it('deleteSrcFolderPics => ', function(){
    //     var names=[];
    //     for(var i = -6; i<=0; i++){
    //         var dayName = (Date.today().add(i).days().toString("yyyy-MM-dd"));
    //         console.log(dayName);
    //         names.push(dayName);
    //     }
    //     console.dir(names);
    //     deleteSrcFolderPics(names);
    // });
    it("delete Public Folder Pic => ", function(){
        deletePublicFolderPics();
    });
});
function copyPicToPublicFolder(_name, _ip){
    var dayName = (Date.today().toString("yyyy-MM-dd"));
    var srcpath = baseDirectory + _ip + "\\" + dayName + "\\" + "01\\" + _name;
    var desPath = "../public/images/cameraImg/" + _name;
    // console.log('destPath => ' + desPath);
    // console.log('src path => ' + srcpath);
    fs_extended.copyFileSync(srcpath, desPath);
    return;
}
function deletePublicFolderPics(){
    var today = Date.today();
    console.log('today => ' + today.toString("yyyy-MM-dd"));
    var desPath = "../public/images/cameraImg/";
    var allFiles = fs.readdirSync(desPath);
    console.dir(allFiles);
    _.each(allFiles, function(_pic){
        var date = parseFileNameToDate(_pic);
        if(date != null){
            console.log(date.toString("yyyy-MM-dd"));
            if(!Date.equals(date.clearTime(), today)){
                console.log(_pic + " should be deleted");
                fs_extended.deleteFileSync(desPath + _pic);
            }
        }
    });
}
function deleteSrcFolderPics(_ignore_folders){
    _.each(pathName_IP, function(_ip){
        var path2IP = baseDirectory + _ip;
        if(fs.existsSync(path2IP)){
            var allFiles = fs.readdirSync(path2IP);
            console.dir(allFiles);
            _.each(allFiles, function(_folder){
                if(!_.contains(_ignore_folders, _folder)){
                    console.log(_folder + ' should be deleted');
                    fs_extended.deleteDirSync(path2IP + "\\" + _folder);
                }
            });
        }        
    });
}
function getNewestFile(_ip){
    var path2IP = baseDirectory + _ip;
    if(fs.existsSync(path2IP)){
        var dayName = (Date.today().toString("yyyy-MM-dd"));
        var path2Date = path2IP + "\\" + dayName;
        if(fs.existsSync(path2Date)){
            var path2Chnl = path2Date + "\\" + "01";
            if(fs.existsSync(path2Chnl)){
                var allFiles = fs.readdirSync(path2Chnl);
                if(_.size(allFiles) > 0){
                    var newestFile = filterNewestFile(allFiles);
                    console.log('the newest file is ' + newestFile);  
                    return newestFile;
                }
                // else{
                //     console.log('no file found!');
                // }
              
            }
        }
    }
    return null    
}

function filterNewestFile(_allFiles){
    var newestFile = _.min(_allFiles, function(_file){
        return getElapsedSeconds(parseFileNameToDate(_file));
    });
    return newestFile;
}
function getElapsedSeconds(_date){
    if(null == _date) return 10000;
    return  (new Date()).getTime()/1000 - _date.getTime()/1000;
}
function parseFileNameToDate(_name){
    if(_name == null) return null;
    var splits = _name.split("_", 3);
    if(_.size(splits) >= 3){
        var timeString = splits[2];
        var time = Date.parse(splits[1], "yyyyMMdd");
        time.setHours(timeString.substr(0,2), timeString.substr(2,2), timeString.substr(4,2));
        return time;
    }
    return null;
}

        // var path2IP = baseDirectory + _ip;
        // if(fs.existsSync(path2IP)){
        //     var dayName = (Date.today().toString("yyyy-MM-dd"));
        //     var path2Date = path2IP + "\\" + dayName;
        //     if(fs.existsSync(path2Date)){
        //         var path2Chnl = path2Date + "\\" + "01";
        //         if(fs.existsSync(path2Chnl)){
        //             var allFiles = fs.readdirSync(path2Chnl);
        //             if(_.size(allFiles) > 0){
        //                 console.log("path => " + path2Chnl);
        //                 console.log("files: ");
        //                 _.each(allFiles, function(_name){
        //                     console.log(_name);
        //                     var seconds = getElapsedSeconds(parseFileNameToDate(_name));
        //                     console.log('time elapsed ' + seconds + 'seconds');
        //                 });

        //                 var newestFile = filterNewestFile(allFiles);
        //                 console.log('the newest file is ' + newestFile);  
        //             }
                  
        //         }
        //     }
        // }

        // var splits = _name.split("_", 3);
        // if(_.size(splits) >= 3){
        //     console.log('date => ' + splits[1]);
        //     console.log('time => ' + splits[2]);
        //     var timeString = splits[2];
        //     var time = Date.parse(splits[1], "yyyyMMdd");
        //     time.setHours(timeString.substr(0,2), timeString.substr(2,2), timeString.substr(4,2));
        //     // console.dir(time.getTime()/1000);
        //     // console.dir((new Date()).getTime()/1000);
        //     // var seconds = (new Date()).getTime()/1000 - time.getTime()/1000;
        //     var seconds = getElapsedSeconds(time);
        //     console.log('time elapsed ' + seconds + 'seconds');
        //     // var time = Date.parse(splits[1] + '-' + splits[2], "yyyyMMdd-HHmmss");
        //     // console.dir(time);
        // }

