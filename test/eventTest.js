require("should");
var _ = require("underscore");

var wEvent = require("../routes/weatherEvent");
var locationMap = require('../routes/locations');


describe("Event Test", function() {
    it("", function() {
        return;
        var para = {locationID: 1, values: {humidity: 41}};
        var results = wEvent.findFitedEvents(para);
        (_.size(results) == 1).should.be.true;
        (results[0].eventID == 101).should.be.true;

        para = {locationID: 1, values: {humidity: 40}};
        results = wEvent.findFitedEvents(para);
        (_.size(results) == 1).should.be.true;
        (results[0].eventID == 102).should.be.true;

        para = {locationID: 1, values: {humidity: 19}};
        results = wEvent.findFitedEvents(para);
        (_.size(results) == 1).should.be.true;
        (results[0].eventID == 103).should.be.true;

        para = {locationID: 1, values: {humidity: 80}};
        results = wEvent.findFitedEvents(para);
        (_.size(results) == 1).should.be.true;
        (results[0].eventID == 104).should.be.true;


        para = {locationID: 1, values: {temperature: 41}};
        results = wEvent.findFitedEvents(para);
        (_.size(results) == 1).should.be.true;
        (results[0].eventID == 201).should.be.true;

        para = {locationID: 1, values: {temperature: 24}};
        results = wEvent.findFitedEvents(para);
        (_.size(results) == 1).should.be.true;
        (results[0].eventID == 202).should.be.true;

        para = {locationID: 1, values: {temperature: 18}};
        results = wEvent.findFitedEvents(para);
        (_.size(results) == 1).should.be.true;
        (results[0].eventID == 203).should.be.true;

        para = {locationID: 1, values: {temperature: 15}};
        results = wEvent.findFitedEvents(para);
        (_.size(results) == 1).should.be.true;
        (results[0].eventID == 204).should.be.true;

        para = {locationID: 1, values: {temperature: -15}};
        results = wEvent.findFitedEvents(para);
        (_.size(results) == 1).should.be.true;
        (results[0].eventID == 205).should.be.true;

        para = {locationID: 1, values: {temperature: -45}};
        results = wEvent.findFitedEvents(para);
        (_.size(results) == 1).should.be.true;
        (results[0].eventID == 206).should.be.true;



        var eventDes = wEvent.getEventDes(para);
        console.dir(eventDes);
        // (wEvent.test.eventID == 1).should.be.true;
    });

    it("" ,function(){
        // return;
        var properties = ['temperature', 'humidity'];
        var node = {nodeId:"32782",temperature:20.55,humidity:14.32};
        // var node = {nodeId:"32782",temperature:"20.55",humidity:"14.32"};
        var locationID = locationMap.getLocationIDWithNodeID(node.nodeId);
        (locationID != -1).should.be.true;
        if(locationID != -1){
            var para = new Object();
            para.locationID = locationID;
            para.values = new Object();
            _.each(properties, function(_property){
                if(null != node[_property]){
                    para.values[_property] = node[_property];
                }
            });
            console.log('event input => ');
            console.dir(para);

            var results = wEvent.findFitedEvents(para);
            (_.size(results) == 2).should.be.true;

            var eventDes = wEvent.getEventDes(para);
            console.log('getEventDes => ');
            console.dir(eventDes);

            // {locationID: 1, values: {humunity: 10, temperature: 20}}
        }        
        return;
    });

    if('normal test', function(){
        var node1 = {nodeId: 1, values:{ temperature: 20.55, humidity: 14.32 }};
        var node2 = {nodeId: 2, values:{ temperature: 20.55, humidity: 14.3 }};
        var node3 = {nodeId: 3, values:{ temperature: 20.55, humidity: 14.32 }};

        (node1.values == node2.values).should.be.false;
        (node1.values == node3.values).should.be.true
    });
});


















