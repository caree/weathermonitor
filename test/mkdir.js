require("should");
var mkdirp = require('mkdirp');
var _ = require("underscore");
var datejs = require("datejs");

var baseDirectory = "C:\\program\\nodejs\\weathermonitor\\testPath\\";
var pathName_IP = ['192.168.1.108', '192.168.1.109', '192.168.1.110', '192.168.1.112', '192.168.1.113'];


describe("create directory", function() {
    it("new directories => ", function() {
        // console.log(Date.today().toString("yyyy-MM-dd"));
        // console.log(Date.today().add(-1).days().toString("yyyy-MM-dd"));
        // return;
        _.each(pathName_IP, function(_ip){
            mkdirp.sync(baseDirectory + _ip, function(err){
                if(err) console.error(err);
            });
        });
        _.each(pathName_IP, function(_ip){
            for(var addDay = -6; addDay <=0; addDay++){
                var day = Date.today().add(addDay).days().toString("yyyy-MM-dd");//HH:mm:ss
                // console.log(day);
                mkdirp.sync(baseDirectory + _ip + "\\" + day, function(err){
                    if(err) console.error(err);
                });
                mkdirp.sync(baseDirectory + _ip + "\\" + day + "\\" + "01", function(err){
                    if(err) console.error(err);
                });
            }             
        });
    });
});

