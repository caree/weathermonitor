require("should");
xml2js = require('xml2js');
var tcpClient = require('../routes/tcpClient');


var eS1100_Soil_Temperature_Sensor_Xml = "Packet><?xml version=\"1.0\" ?><MotePacket><PacketName>Soil Temperature Sensor</PacketName><NodeId>4.000000</NodeId><Port>2.000000</Port><ParsedDataElement><Name>amType</Name><ConvertedValue>11</ConvertedValue><ConvertedValueType>uint8</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>group</Name><ConvertedValue>83</ConvertedValue><ConvertedValueType>uint8</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>nodeId</Name><SpecialType>nodeid</SpecialType><ConvertedValue>32778</ConvertedValue><ConvertedValueType>uint16</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>socketId</Name><ConvertedValue>52</ConvertedValue><ConvertedValueType>uint8</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>boardId</Name><SpecialType>sensorboardid</SpecialType><ConvertedValue>188</ConvertedValue><ConvertedValueType>uint8</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>packetId</Name><ConvertedValue>0</ConvertedValue><ConvertedValueType>uint8</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>temp</Name><ConvertedValue>24.062500</ConvertedValue><ConvertedValueType>float</ConvertedValueType></ParsedDataElement><internal><nodeId>32778.000000</nodeId><sensorDeviceParentNodeId>4.000000</sensorDeviceParentNodeId><sensorDeviceSubAddress>2.000000</sensorDeviceSubAddress><sensorDeviceSensorId>188.000000</sensorDeviceSensorId><sensorTable>eS1500_sensor_results</sensorTable></internal></MotePacket>";
var eS1100_Soil_Moisture_Sensor_xml = "<?xml version=\"1.0\" ?><MotePacket><PacketName>eS1100 Soil Moisture Sensor v1</PacketName><NodeId>4.000000</NodeId><Port>1.000000</Port><ParsedDataElement><Name>amType</Name><ConvertedValue>11</ConvertedValue><ConvertedValueType>uint8</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>group</Name><ConvertedValue>83</ConvertedValue><ConvertedValueType>uint8</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>nodeId</Name><SpecialType>nodeid</SpecialType><ConvertedValue>32777</ConvertedValue><ConvertedValueType>uint16</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>socketId</Name><ConvertedValue>52</ConvertedValue><ConvertedValueType>uint8</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>boardId</Name><SpecialType>sensorboardid</SpecialType><ConvertedValue>23</ConvertedValue><ConvertedValueType>uint8</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>packetId</Name><ConvertedValue>0</ConvertedValue><ConvertedValueType>uint8</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>soilMoisture</Name><ConvertedValue>0.338956</ConvertedValue><ConvertedValueType>float</ConvertedValueType></ParsedDataElement><internal><nodeId>32777.000000</nodeId><sensorDeviceParentNodeId>4.000000</sensorDeviceParentNodeId><sensorDeviceSubAddress>1.000000</sensorDeviceSubAddress><sensorDeviceSensorId>23.000000</sensorDeviceSensorId><sensorTable>eS1100_sensor_results</sensorTable></internal></MotePacket>";
var ES1201AmbientTemperatureAndHumiditySensor_xml = "<?xml version=\"1.0\" ?><MotePacket><PacketName>eS1201 Ambient Temperature and Humidity Sensor</PacketName><NodeId>6.000000</NodeId><Port>4.000000</Port><ParsedDataElement><Name>amType</Name><ConvertedValue>11</ConvertedValue><ConvertedValueType>uint8</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>group</Name><ConvertedValue>83</ConvertedValue><ConvertedValueType>uint8</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>nodeId</Name><SpecialType>nodeid</SpecialType><ConvertedValue>32775</ConvertedValue><ConvertedValueType>uint16</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>socketId</Name><ConvertedValue>52</ConvertedValue><ConvertedValueType>uint8</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>boardId</Name><SpecialType>sensorboardid</SpecialType><ConvertedValue>17</ConvertedValue><ConvertedValueType>uint8</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>packetId</Name><ConvertedValue>0</ConvertedValue><ConvertedValueType>uint8</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>temperature</Name><ConvertedValue>31.830000</ConvertedValue><ConvertedValueType>float</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>humidity</Name><ConvertedValue>43.816799</ConvertedValue><ConvertedValueType>float</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>dewPoint</Name><ConvertedValue>17.977972</ConvertedValue><ConvertedValueType>float</ConvertedValueType></ParsedDataElement><internal><nodeId>32775.000000</nodeId><sensorDeviceParentNodeId>6.000000</sensorDeviceParentNodeId><sensorDeviceSubAddress>4.000000</sensorDeviceSubAddress><sensorDeviceSensorId>17.000000</sensorDeviceSensorId><sensorTable>eS1201_sensor_results</sensorTable></internal></MotePacket>";
var ET22_Weather_Sensor_xml = "<?xml version=\"1.0\" ?><MotePacket><PacketName>ET22 Weather Sensor</PacketName><NodeId>7.000000</NodeId><Port>4.000000</Port><ParsedDataElement><Name>amType</Name><ConvertedValue>11</ConvertedValue><ConvertedValueType>uint8</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>group</Name><ConvertedValue>83</ConvertedValue><ConvertedValueType>uint8</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>nodeId</Name><SpecialType>nodeid</SpecialType><ConvertedValue>32770</ConvertedValue><ConvertedValueType>uint16</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>socketId</Name><ConvertedValue>52</ConvertedValue><ConvertedValueType>uint8</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>boardId</Name><SpecialType>sensorboardid</SpecialType><ConvertedValue>22</ConvertedValue><ConvertedValueType>uint8</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>packetId</Name><ConvertedValue>0</ConvertedValue><ConvertedValueType>uint8</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>WindLastCnt</Name><ConvertedValue>2</ConvertedValue><ConvertedValueType>uint16</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>WindMaxCnt</Name><ConvertedValue>4</ConvertedValue><ConvertedValueType>uint16</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>WindSampleCnt</Name><ConvertedValue>565</ConvertedValue><ConvertedValueType>uint16</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>DirLastCnt</Name><ConvertedValue>799</ConvertedValue><ConvertedValueType>uint16</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>DirSampleCnt</Name><ConvertedValue>730</ConvertedValue><ConvertedValueType>uint16</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>RainSampleCnt</Name><ConvertedValue>0</ConvertedValue><ConvertedValueType>uint16</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>RainTotalCnt</Name><ConvertedValue>1300</ConvertedValue><ConvertedValueType>uint16</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>TimerCnt</Name><ConvertedValue>489</ConvertedValue><ConvertedValueType>uint16</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>TempCnt</Name><ConvertedValue>6316</ConvertedValue><ConvertedValueType>uint16</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>HumidityCnt</Name><ConvertedValue>1940</ConvertedValue><ConvertedValueType>uint16</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>SolarCnt</Name><ConvertedValue>279</ConvertedValue><ConvertedValueType>uint16</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>UVCnt</Name><ConvertedValue>1023</ConvertedValue><ConvertedValueType>uint16</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>AdcRefCnt</Name><ConvertedValue>391</ConvertedValue><ConvertedValueType>uint16</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>BPCnt</Name><ConvertedValue>10104</ConvertedValue><ConvertedValueType>uint16</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>TempIntCnt</Name><ConvertedValue>275</ConvertedValue><ConvertedValueType>uint16</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>WindLast</Name><ConvertedValue>3.621024</ConvertedValue><ConvertedValueType>float</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>WindMax</Name><ConvertedValue>7.242048</ConvertedValue><ConvertedValueType>float</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>WindAvg</Name><ConvertedValue>2.091900</ConvertedValue><ConvertedValueType>float</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>WindDir</Name><ConvertedValue>281.173035</ConvertedValue><ConvertedValueType>float</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>WindDirAvg</Name><ConvertedValue>256.891510</ConvertedValue><ConvertedValueType>float</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>Rain</Name><ConvertedValue>0.000000</ConvertedValue><ConvertedValueType>float</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>RainRate</Name><ConvertedValue>0.000000</ConvertedValue><ConvertedValueType>float</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>RainTotal</Name><ConvertedValue>33.020000</ConvertedValue><ConvertedValueType>float</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>Temp</Name><ConvertedValue>23.410000</ConvertedValue><ConvertedValueType>float</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>Humidity</Name><ConvertedValue>64.031921</ConvertedValue><ConvertedValueType>float</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>Solar</Name><ConvertedValue>523.416077</ConvertedValue><ConvertedValueType>float</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>BP</Name><ConvertedValue>1010.400024</ConvertedValue><ConvertedValueType>float</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>TempInt</Name><ConvertedValue>27.500000</ConvertedValue><ConvertedValueType>float</ConvertedValueType></ParsedDataElement><ParsedDataElement><Name>DewPoint</Name><ConvertedValue>16.210352</ConvertedValue><ConvertedValueType>float</ConvertedValueType></ParsedDataElement><internal><nodeId>32770.000000</nodeId><sensorDeviceParentNodeId>7.000000</sensorDeviceParentNodeId><sensorDeviceSubAddress>4.000000</sensorDeviceSubAddress><sensorDeviceSensorId>22.000000</sensorDeviceSensorId><sensorTable>eS2000_sensor_results</sensorTable></internal></MotePacket>";

var totalSensor_xml = eS1100_Soil_Temperature_Sensor_Xml + eS1100_Soil_Moisture_Sensor_xml + ES1201AmbientTemperatureAndHumiditySensor_xml+ ET22_Weather_Sensor_xml;


describe('demo test => ', function(){
	it('named para test =>', function(){
		var strs=[];
		 strs['str1'] = 'str1'; 			
		 strs['str2'] = 'str2';
		console.log('str1 => ' + strs['str1']); 			
		(strs['str3'] == null).should.be.true; 			
		console.log('str3 => ' + strs['str3']);
	});
	it("Node =>", function(){
		node = tcpClient.initialNode({nodeId:'id001', temperature:23, humidity:33});
		// ((node.nodeId == 'id001') && (node.temperature == 23)).should.be.true;
		node.update({nodeId:'id002', temperature:43, humidity:53});
		(node.temperature == 23).should.be.true;

		var b = node.update({nodeId:'id001', temperature:23, humidity:33});
		(false == b).should.be.true;

		b = node.update({nodeId:'id001', temperature:43, humidity:33});
		(true == b).should.be.true;
		(node.temperature == 43).should.be.true;
		(node.humidity == 33).should.be.true;

	});
	it("String => ", function(){
		var data = '111111</MotePacket>22222222222222</MotePacket>4444';
		var index = data.lastIndexOf('</MotePacket>');
		// console.log('lastIndex => '+ index);
		(index == 33).should.be.true;
		var sub = data.substring(0, index+13);
		// console.log('substring =>');
		// console.log(sub);
	});
	if('', function(){
		var p = parserList['111'];
		(p == null).should.be.true;
	});	
});


describe('RawData Parse => ', function(){
	
	it('', function(){
		var sensors = totalSensor_xml.match(/\<MotePacket\>[\<\w\> \.\/]+\<\/MotePacket\>/g);
		(sensors.length == 4).should.be.true;

		// console.log('match result =>')
		// console.dir(sensors);
		for (var i = 0; i < sensors.length; i++) {
			var s = sensors[i];
			tcpClient.ParseNodeXml(s, function(packetName, node){
				if(packetName == 'Soil Temperature Sensor'){
					(null == node).should.be.false;
					(node.nodeId == '32778').should.be.true;
					(node.soilTemperature == '24.06').should.be.true;
				}
				if(packetName == 'eS1100 Soil Moisture Sensor v1'){
					(null == node).should.be.false;
					(node.nodeId == '32777').should.be.true;
					(node.soilMoisture == '0.34').should.be.true;					
				}
				if(packetName == 'eS1201 Ambient Temperature and Humidity Sensor'){
					(null == node).should.be.false;
					(node.nodeId == '32775').should.be.true;
					(node.temperature == '31.83').should.be.true;
					(node.humidity == '43.82').should.be.true;					
				}
				if(packetName == 'ET22 Weather Sensor'){
					(null == node).should.be.false;
					(node.nodeId == '32770').should.be.true;
					(node.Solar == '523.42').should.be.true;
					(node.WindMax == '7.24').should.be.true;
					(node.WindAvg == '2.09').should.be.true;
					(node.WindDirAvg == '256.89').should.be.true;
					(node.RainRate == '0.00').should.be.true;
					(node.BP == '1010.40').should.be.true;
					(node.temperature == '23.41').should.be.true;
					(node.humidity == '64.03').should.be.true;					
				}												
			});
		};
	});

});


