

max_pic_update_interval = 60;//second

baseDirectory = "C:\\program\\nodejs\\weathermonitor\\testPath\\";
pathName_IP = ['192.168.1.108', '192.168.1.109', '192.168.1.110', '192.168.1.112', '192.168.1.113'];
ip_id_map_list = [{ip: '192.168.1.108', id: '10001'}, {ip: '192.168.1.109', id: '10002'}
            , {ip: '192.168.1.110', id: '10003'}, {ip: '192.168.1.112', id: '10004'}
            , {ip: '192.168.1.113', id: '10005'}];

G_itemList = [{itemID: 'bwu', img: 'bwu.png', mainText: '北京物资学院', subText: '北京市通州区富河大街1号'}, 
        {itemID: 'bnu', img: 'bnu.png', mainText: '北京师范大学', subText: '北京市新街口外大街19号'}];

G_markParaList = [{id: "bwu_map_5", label: "信息学院外气象站", lat: 12983419.43359375, lng: 4827632.8125, itemID: 'bwu'},
                {id: "bwu_map_6", label: "信息学院办公室", lat: 12983470.703125, lng: 4827646.484375, itemID: 'bwu'},
                {id: "bnu_map_1", label: "科技楼", lat: 12953235.69052698, lng: 4832523.763818118, itemID: 'bnu'},
                {id: "bnu_map_2", label: "学十五楼", lat: 12953258.371106194, lng: 4832013.396833899, itemID: 'bnu'},
                {id: "bnu_map_3", label: "英东楼", lat: 12953422.943813816, lng: 4832045.246731605, itemID: 'bnu'},
                {id: "bnu_map_4", label: "东体育场", lat: 12953052.038852531, lng: 4832443.172278292, itemID: 'bnu'}
                ];


/**
 * Module dependencies.
 */
var colors = require('colors');

colors.setTheme({
  silly: 'rainbow',
  input: 'grey',
  verbose: 'cyan',
  prompt: 'grey',
  info: 'green',
  data: 'grey',
  help: 'cyan',
  warn: 'yellow',
  debug: 'blue',
  error: 'red'
});
require('./routes/tcpClient');
var nodeMap = require('./routes/nodeMap');
var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var path = require('path');
var webSocketServer = require('./routes/wsServer');
var app = express();

var server = webSocketServer.startWebSocketServer(app);
// all environments
app.set('port', process.env.PORT || 6013);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}



app.get('/', routes.index);
app.get('/wsTest', routes.wsTest);
app.get('/wsTest_ie10', routes.wsTest_ie10);
app.get('/map', routes.map);
app.get('/bwumap', routes.bwumap);
app.get('/allmap', routes.allmap);
app.get('/index.php/Mobile/index_mobile_map', routes.allmap);

app.get('/nodeInfo/:nodeID', routes.nodeInfo);
app.get('/ajaxGetNodeInfo', routes.ajaxGetNodeInfo);
app.get('/index_realtime_camera_full/:id', routes.realtime_camera_full);
app.get('/getNewestPicName/:id', routes.getNewestPicName);
app.get('/locationID/:locationID', routes.locationID);

// app.get('/users', user.list);
app.get('/index', routes.eventInfoIndex);
app.get('/eventInfo', routes.eventInfo);
app.get('/ajaxGetEvents', routes.ajaxGetEvents);

server.listen(app.get('port'), function(){
  console.log(('Weather Monitor listening on port ' + app.get('port')).info);
});
