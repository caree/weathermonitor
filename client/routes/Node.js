


function Node(options){
	// console.log('Node => ')
    options = options || {};  
    for(var opt in options){
    	if(this.hasOwnProperty(opt)){
    		// console.log('Node has property ' + opt);
    		this[opt] = options[opt];
    		// console.log('Node property change to ' + options[opt]);
    	}else{
    		// console.log('Node has no property ' + opt);
	    	this[opt] = options[opt];
	    	// console.log('Node new property ' + opt + '\'s value is ' + options[opt]);
    	}
    }
}
Node.prototype.update = function(node){
	var bUpdated = false;
	if(this.nodeId != node.nodeId) return;
	for(var p in node){
		// console.log('this:' + p + ' => ' + this[p]);
		// console.log('node:' + p + ' => ' + node[p]);
		if(!this.hasOwnProperty(p)){
			// console.log(p + '  updated  to ' + node[p]);
			this[p] = node[p];
		}else{
			if(p == 'nodeId') continue;
			if(this[p] != node[p]){
				// console.log(p + '  updated from ' + this[p] + ' to ' + node[p]);
				this[p] = node[p];
				bUpdated = true;
			}
		}
	}
	return bUpdated;
}

module.exports = Node;