


var Log = require('log')
  , log = new Log('info');

var _ = require("underscore");
var timeFormater = require('./timeFormat').getCurrentTime;
var xml2js = require('xml2js');
var net = require('net');
var Node = require('./Node');
//*****************************************************************************

var client ;
var dataTemp = '';

global.nodeList= [];

var parserList = new Array();
parserList['Soil Temperature Sensor'] = ParseNodeXml_SoilTemperatureSensor;
parserList['eS1100 Soil Moisture Sensor v1'] = ParseNodeXml_SoilMoistureSensor;
parserList['eS1201 Ambient Temperature and Humidity Sensor'] = ParseNodeXml_eS1201AmbientTemperatureAndHumiditySensor;
parserList['ET22 Weather Sensor'] = ParseNodeXml_ET22WeatherSensor;
parserList['eS1401 Solar Radiation Sensor v2'] = ParseNodeXml_eS1401SolarRadiationSensor_v2;
parserList['default'] = ParseNodeXml_default;


//*****************************************************************************

exports.startTcpClient = startTcpClient;
exports.checkTcpClientState = checkTcpClientState;

//*****************************************************************************

function startTcpClient(_info){

	if(_info != null){
		log.info(timeFormater() + '  startTcpClient => ' + _info);
		// log.debug('preparing email');
	}else{
		log.info(timeFormater() + '  startTcpClient => ');
	}
	client = new net.Socket();
	client.connect(PORT, HOST, function() {

	    // Write a message to the socket as soon as the client is connected, the server will receive it as message from the client 
	    // client.write('I am Chuck Norris!');

	});
	client.on('connect', function(_socket){
	    console.log(timeFormater() + '  CONNECTED TO: ' + HOST + ':' + PORT);
	});
	// Add a 'data' event handler for the client socket
	// data is what the server sent to this socket
	client.on('data', function(data) {
	    
		   parseRawData(String(data).replace(/[\r\n]/g, ''));

	});
	client.on('drain',function(){
		console.info('tcp client drain ...');
		destroyClient();
	});
	client.on('end', function(){
		console.info('tcp client end ...');
		destroyClient();
	});
	client.on('timeout', function(){
		console.info('tcp client timeout ...');
		destroyClient();
	});
	client.on('error', function(error){
		console.error('tcp client error ...');
		destroyClient();
	});
	// Add a 'close' event handler for the client socket
	client.on('close', function() {
		console.info('tcp client close ...');
		destroyClient();
	});	
}
function checkTcpClientState(){
	if(client == null){
		startTcpClient('restart TCP Client ...');
	}else{
		console.log('TCP Client running ...');
	}
}

function destroyClient(){
	if(client != null){
		client.destroy();
		client = null;
	} 
}

function parseRawData(data){
	// log.info(data);
	// dataTemp += data.replace(/\n/g, '');
	dataTemp += data;
	var index = dataTemp.lastIndexOf('</MotePacket>');
	if(index < 0) return;
	var strToParse = dataTemp.substring(0, index+13);
	dataTemp = dataTemp.substring(index+13);
	// console.log('strToParse => ' +strToParse);
	var sensors = strToParse.match(/\<MotePacket\>[\<\w\>\s\.\/\-]+\<\/MotePacket\>/g);
	if(sensors == null){
		// console.log('no packet is available !!!');
		return;
	} 
	// console.log('match result => ' + sensors.length);
	// console.dir(sensors);
	for (var i = 0; i < sensors.length; i++) {
		var s = sensors[i];
		ParseNodeXml(s, function(packetName, node){
			// console.dir(node);
			// updateNodeList(node);
			ep.emit('weatherNodeInfo', node);
		});

	}	
}

function updateNodeList(node){
	if(node == null) return;

	if(findNode(node) != null){
		var b = findNode(node).update(node);
		if(true == b){
			// todo broadcast
			console.log(timeFormater() + '  node update => ');
			var str = JSON.stringify([node]);
			console.log(str);
			broadcastNodeInfo(str);
			// console.log('********************************');
			// console.log(JSON.stringify(global.nodeList));
		}
	}else{
		global.nodeList.push(node);
		console.log(timeFormater() + '  new node => ');
		var str = JSON.stringify([node]);
		console.log(str);
		broadcastNodeInfo(str);
		// todo broadcast		
		// console.log('********************************');
		// console.log(JSON.stringify(global.nodeList));
		// console.dir(global.nodeList);

	}
}

function broadcastNodeInfo(msg){
	var obj = {name:'nodes', content:msg};
	global.wss.broadcast(JSON.stringify(obj));
}
function findNode(node){
	for(var n in global.nodeList){
		if(global.nodeList[n].nodeId == node.nodeId){
			return global.nodeList[n];
		}
	}
	return null;
}

exports.initialNode = function(options){
	return new Node(options);
};
function ParseNodeXml(xml,callback){
	var parser = new xml2js.Parser();
	 parser.parseString(xml, function (err, result) {
	 	if(err != null){
	 		return null;
	 	}
	 	var packetName = result.MotePacket.PacketName[0];
	 	// console.log(xml);
	 	// console.log('packetName => ' + packetName);
	 	if(parserList[packetName] != null){
		 	// 查找解析函数，返回Node给接收函数
		 	parserList[packetName](packetName, result.MotePacket.ParsedDataElement, function(node){
		 		callback(packetName, node);
		 	});
	 	}else{
	 		parserList['default'](packetName);
	 	}
    });
}
exports.ParseNodeXml = function(xml, callback){
	ParseNodeXml(xml, callback);
}
function ParseNodeXml_default(packetName, elements, callback){
		console.log(timeFormater() + '  received packet ' + packetName);
	}
function ParseNodeXml_ET22WeatherSensor(packetName, elements, callback){
	    if(packetName != 'ET22 Weather Sensor') return;
	    if(elements == null) return;
	    var obj = new Object;

	    for (var i = 0; i < elements.length; i++) {
	    	var element = elements[i];
	    	// console.log(element.Name + ' => ' + element.ConvertedValue);
	    	if(element.Name == 'nodeId'){
	    		obj.nodeId = element.ConvertedValue[0];
	    	}
	    	if(element.Name == 'Temp'){
	    		var num = new Number(element.ConvertedValue[0]);
	    		obj.temperature = num.toFixed(2);
	    		// obj.temperature = (element.ConvertedValue[0]).toFixed(2);
	    	}
	    	if(element.Name == 'Humidity'){
	    		var num = new Number(element.ConvertedValue[0]);
	    		obj.humidity = num.toFixed(2);
	    		// obj.humidity = element.ConvertedValue[0];
	    	}	
	    	if(element.Name == 'Solar'){
	    		var num = new Number(element.ConvertedValue[0]);
	    		obj.Solar = num.toFixed(2);
	    		// obj.Solar = element.ConvertedValue[0];
	    	}	
	    	if(element.Name == 'WindAvg'){
	    		var num = new Number(element.ConvertedValue[0]);
	    		obj.WindAvg = num.toFixed(2);	    		
	    		// obj.WindAvg = element.ConvertedValue[0];
	    	}		
	    	if(element.Name == 'WindMax'){
	    		var num = new Number(element.ConvertedValue[0]);
	    		obj.WindMax = num.toFixed(2);	    		
	    		// obj.WindMax = element.ConvertedValue[0];
	    	}	
	    	if(element.Name == 'WindDirAvg'){
	    		var num = new Number(element.ConvertedValue[0]);
	    		obj.WindDirAvg = num.toFixed(2);	    		
	    		// obj.WindDirAvg = element.ConvertedValue[0];
	    	}		  
	    	if(element.Name == 'RainRate'){
	    		var num = new Number(element.ConvertedValue[0]);
	    		obj.RainRate = num.toFixed(2);	    		
	    		// obj.RainRate = element.ConvertedValue[0];
	    	}	
	    	if(element.Name == 'BP'){
	    		var num = new Number(element.ConvertedValue[0]);
	    		obj.BP = num.toFixed(2);	    		
	    		// obj.BP = element.ConvertedValue[0];
	    	}		    		    	  		    	    	    	
	    	if(element.Name == 'RainTotal'){
	    		var num = new Number(element.ConvertedValue[0]);
	    		obj.RainTotal = num.toFixed(2);	    		
	    		// obj.RainTotal = element.ConvertedValue[0];
	    	}		    		    	  		    	    	    	
	    };
	    var node = new Node(obj);
	    // console.dir(node);
	    callback(node);
		// return node;
    // }
    // );			
}
function ParseNodeXml_eS1201AmbientTemperatureAndHumiditySensor(packetName, elements, callback){
    // ParseNodeXml(xml, function(packetName, elements){
	    // console.dir(elements);
	    if(packetName != 'eS1201 Ambient Temperature and Humidity Sensor') return;
	    if(elements == null) return;
	    var obj = new Object;

	    for (var i = 0; i < elements.length; i++) {
	    	var element = elements[i];
	    	// console.log(element.Name + ' => ' + element.ConvertedValue);
	    	if(element.Name == 'nodeId'){
	    		obj.nodeId = element.ConvertedValue[0];
	    	}
	    	if(element.Name == 'temperature'){
	    		var num = new Number(element.ConvertedValue[0]);
	    		obj.temperature = num.toFixed(2);	    		

	    		// obj.temperature = element.ConvertedValue[0];
	    	}
	    	if(element.Name == 'humidity'){
	    		var num = new Number(element.ConvertedValue[0]);
	    		obj.humidity = num.toFixed(2);	    		

	    		// obj.humidity = element.ConvertedValue[0];
	    	}	    	
	    };
	    var node = new Node(obj);
	    // console.dir(node);
	    callback(node);
		// return node;
    // });		
}
function ParseNodeXml_SoilMoistureSensor(packetName, elements, callback){
    // ParseNodeXml(xml, function(packetName, elements){
	    // console.dir(elements);
	    if(packetName != 'eS1100 Soil Moisture Sensor v1') return;
	    if(elements == null) return;
	    var obj = new Object;

	    for (var i = 0; i < elements.length; i++) {
	    	var element = elements[i];
	    	// console.log(element.Name + ' => ' + element.ConvertedValue);
	    	if(element.Name == 'nodeId'){
	    		obj.nodeId = element.ConvertedValue[0];
	    	}
	    	if(element.Name == 'soilMoisture'){
	    		// obj.soilMoisture = element.ConvertedValue[0];
	    		var num = new Number(element.ConvertedValue[0]);
	    		obj.soilMoisture = num.toFixed(2);	    		

	    	}
	    };
	    var node = new Node(obj);
	    // console.dir(node);
	    callback(node);
		// return node;
    // });	
}
function ParseNodeXml_SoilTemperatureSensor(packetName, elements, callback){
    // ParseNodeXml(xml, function(packetName, elements){
	    // console.dir(elements);
	    if(packetName != 'Soil Temperature Sensor') return;
	    if(elements == null) return;
	    var obj = new Object;

	    for (var i = 0; i < elements.length; i++) {
	    	var element = elements[i];
	    	// console.log(element.Name + ' => ' + element.ConvertedValue);
	    	if(element.Name == 'nodeId'){
	    		obj.nodeId = element.ConvertedValue[0];
	    	}
	    	if(element.Name == 'temp'){
	    		var num = new Number(element.ConvertedValue[0]);
	    		obj.soilTemperature = num.toFixed(2);	    		

	    		// obj.soilTemperature = element.ConvertedValue[0];
	    	}
	    };
	    var node = new Node(obj);
	    // console.dir(node);
	    callback(node);
		// return node;
    // });
}
function ParseNodeXml_eS1401SolarRadiationSensor_v2(packetName, elements, callback){
	if(packetName != 'eS1401 Solar Radiation Sensor v2') return;
	if(elements == null) return;
	    var obj = new Object;

	    for (var i = 0; i < elements.length; i++) {
	    	var element = elements[i];
	    	// console.log(element.Name + ' => ' + element.ConvertedValue);
	    	if(element.Name == 'nodeId'){
	    		obj.nodeId = element.ConvertedValue[0];
	    	}
	    	if(element.Name == 'solarRadiation'){
	    		var num = new Number(element.ConvertedValue[0]);
	    		obj.solarRadiation = num.toFixed(2);	    		
	    	}
	    };
	    var node = new Node(obj);
	    // console.dir(node);
	    callback(node);
		// return node;
    // });
}
