
var EventProxy = require('eventproxy');
ep = new EventProxy();
var WebSocket = require('faye-websocket');
var tcpClient = require('./routes/tcpClient');

/**
 * Module dependencies.
 */
HOST = '192.168.48.60';
// HOST = '172.16.180.10';
 // HOST = '127.0.0.1';
//  PORT = 6969;
PORT = 9005;
LOCATION_ID = 'bwu';//用于分辨不同地区具有相同编号的数据

var wsURL_Center = 'ws://127.0.0.1:6013';
// var wsURL_Center = 'ws://192.168.48.109:6013';
var websocket;

//*****************************************************************************

ep.tail('weatherNodeInfo', function(_node){
	if(_node == null) return;
	if(websocket != null){
		_node.locationID = LOCATION_ID;
		_node.nodeId = LOCATION_ID + '_' + _node.nodeId;
		var obj = {cmd: 'weatherNodeInfo', data: (JSON.stringify(_node))}
		websocket.send(JSON.stringify(obj));
	}else{
		console.error('connection error, data can not be sent!!!');
	}
});
createWebSocket();
tcpClient.startTcpClient(null);


setInterval(tcpClient.checkTcpClientState, 15000);

setInterval(function(){
	if(websocket == null){
		createWebSocket();
	}else{
		console.log('websocket runs well! ');
	}
}, 15000);
// ep.tail()

//*****************************************************************************


function createWebSocket() {
    console.log('createWebSocket =>');
    websocket = new WebSocket.Client(wsURL_Center);
    websocket.on('open', function (evt) { onOpen(evt) });
    websocket.on('close', function (evt) { onClose(evt) });
    websocket.on('message', function (evt) {onMessage(evt)});
    websocket.on('error', function (evt) { onError(evt) });
}
function onOpen(evt) {}
function onClose(evt) {
	websocket = null;
}
function onError(evt){
    websocket = null;
}
function onMessage(evt) {}
function onNewCommand(cmds){}

